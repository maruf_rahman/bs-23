<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<jsp:include page="navigation.jsp" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Home</title>
<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body ng-app="travel" ng-controller="HomeController">

	<div class="container">
		<table class="table table-striped">
			<thead>
				<tr>
					<td></td>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="row in PostDtoList">
					<td>{{row.content}}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${contextPath}/resources/js/lib/angular.min.js"></script>
	<script
		src="${contextPath}/resources/js/lib/ui-bootstrap-tpls-2.5.0.min.js"></script>
	<script src="${contextPath}/resources/js/app/app.js"></script>
	<script src="${contextPath}/resources/js/app/HomeController.js"></script>
	<script src="${contextPath}/resources/js/app/HomeService.js"></script>
</body>
</html>

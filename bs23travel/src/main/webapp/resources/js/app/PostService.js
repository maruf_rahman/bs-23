
angular.module('travel.services', []).factory('PostService',
		[ "$http", "CONSTANTS", function($http, CONSTANTS) {
			var service = {};
			service.getPostById = function(PostId) {
				var url = CONSTANTS.getPostById + PostId;
				return $http.get(url);
			}
			service.getAllPosts = function() {
				return $http.get(CONSTANTS.getAllPosts);
			}
			service.savePost = function(postDto) {
				return $http.post(CONSTANTS.savePost, postDto);
			}
			service.pinPost = function(PostDto) {
				var url = CONSTANTS.pinPost;
				PostDto = JSON.stringify(PostDto);
				return $http.post(url, PostDto);
			}
			return service;
		} ]);
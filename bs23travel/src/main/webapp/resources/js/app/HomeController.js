var module = angular.module('travel.controllers', []);
module.controller("HomeController", [ "$scope", "HomeService",
		function($scope, HomeService) {

			$scope.PostDtoList = [];

			$scope.getPostList = function() {
				HomeService.getAllPosts().then(function(value) {
					$scope.PostDtoList = value.data;
				}, function(error) {
					console.log(error);
				});
			};
			
			$scope.getPostList();
		} ]);
var module = angular.module('travel.controllers', []);
module.controller("PostController", [ "$scope", "PostService",
		function($scope, PostService) {

			$scope.PostDto = {
				id : null,
				content : null,
				locationID : null,
				locationName : '',
				privacyID : null,
				privacyName : '',
				pinPost : null,
				date : null,
				initialize : function(data) {
					id = data ? data.id : '';
					content = data ? data.content || '' : '';
					locationID = data ? data.locationID || '' : '';
					locationName = data ? data.locationName || '' : '';
					privacyID = data ? data.privacyID || '' : '';
					privacyName = data ? data.privacyName || '' : '';
					pinPost = data ? data.pinPost || '' : '';
				}
			};
			
			$scope.PostDtoList = [];

			$scope.getPostByID = function(postID) {
				PostService.getPostById(postID).then(function(res) {
					$scope.PostDto.initialize(res.data);
				}, function(error) {
					console.log(error);
				});
			};

			$scope.savePost = function() {
				PostService.savePost($scope.PostDto).then(function() {
					$scope.reset();
				}, function(error) {
					console.log(error);
				}).then(function() {
					$scope.getPostList();
				});
			};

			$scope.reset = function(){
				$scope.PostDto.id = null;
				$scope.PostDto.content = null;
				$scope.PostDto.locationID = null;
				$scope.PostDto.privacyID = null;
			};
			
			$scope.edit = function(PostDto) {
				$scope.PostDto.id=PostDto.id;
				$scope.PostDto.content=PostDto.content;
				$scope.PostDto.locationID=PostDto.locationID + '';
				$scope.PostDto.privacyID=PostDto.privacyID + '';
			};

			$scope.pinPost = function(PostDto) {
				PostDto.location = null;
				PostDto.privacy = null;
				PostDto.date = null;
				PostService.pinPost(PostDto).then(function() {
				}, function(error) {
					console.log(error);
				}).then(function() {
					$scope.getPostList();
				});
			};

			$scope.getPostList = function() {
				PostService.getAllPosts().then(function(value) {
					$scope.PostDtoList = value.data;
				}, function(error) {
					console.log(error);
				});
			};
			
			$scope.getPostList();
		} ]);
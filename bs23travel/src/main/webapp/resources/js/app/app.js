var travelApp = angular.module('travel', [ 'ui.bootstrap',
		'travel.controllers', 'travel.services' ]);
travelApp.constant("CONSTANTS", {
	getPostById : "/post/",
	pinPost : "/post/pin",
	getAllPosts : "/post/list",
	getAllHomePosts : "/home/post/list",
	savePost : "/post/save"
});
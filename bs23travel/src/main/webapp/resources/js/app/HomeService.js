
angular.module('travel.services', []).factory('HomeService',
		[ "$http", "CONSTANTS", function($http, CONSTANTS) {
			var service = {};
			service.getAllPosts = function() {
				return $http.get(CONSTANTS.getAllHomePosts);
			}
			return service;
		} ]);
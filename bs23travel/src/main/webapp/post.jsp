<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Profile</title>
<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<form id="logoutForm" method="POST" action="${contextPath}/logout">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>


			<h2 style="float: left;">Welcome ${pageContext.request.userPrincipal.name} </h2>
			<h2 style="float: right"><a onclick="document.forms['logoutForm'].submit()">Logout</a></h2>
			
		</c:if>
	</div>

	<div class="container">
		<form method="POST" class="form-signin" ng-app="travel"
			ng-controller="PostController" id="postForm" ng-submit="savePost()">
			<h2 class="form-signin-heading">What's on your mind?</h2>
			<div class="form-group">
				<input type="text" class="form-control" ng-model="PostDto.content"
					ng-required="true" placeholder="Write Your Post..." ng-focus="true"></input>
			</div>

			<div class="form-group">
				<form:select path="locationList" class="form-control"
					ng-required="true" ng-model="PostDto.locationID">
					<form:option value="">Please Select</form:option>
					<form:options items="${locationList}" itemValue="id"
						itemLabel="name" />
				</form:select>
			</div>

			<div class="form-group">
				<form:select path="privacyList" class="form-control"
					ng-required="true" ng-model="PostDto.privacyID">
					<form:option value="">Please Select</form:option>
					<form:options items="${privacyList}" itemValue="id"
						itemLabel="name" />
				</form:select>
			</div>

			<button class="btn btn-lg btn-primary btn-block">Share</button>

			<input type="button" value="Reset" ng-click="reset()"
				class="btn btn-lg btn-primary btn-block" />

			<div class="form-group">
				<table class="table table-striped">
					<thead>
						<tr>
							<td>SL</td>
							<td>Post</td>
							<td>Location</td>
							<td>Privacy</td>
							<td>Pin</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="row in PostDtoList">
							<td>{{$index+1}}</td>
							<td>{{row.content}}</td>
							<td>{{row.locationName}}</td>
							<td>{{row.privacyName}}</td>
							<td><input type="checkbox" ng-checked="row.pinPost"
								ng-click="pinPost(row)" ng-disabled="!row.editable"></td>
							<td><input type="button" ng-click="edit(row)"
								ng-show="row.editable" value="Edit"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</form>

	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${contextPath}/resources/js/lib/angular.min.js"></script>
	<script
		src="${contextPath}/resources/js/lib/ui-bootstrap-tpls-2.5.0.min.js"></script>
	<script src="${contextPath}/resources/js/app/app.js"></script>
	<script src="${contextPath}/resources/js/app/PostController.js"></script>
	<script src="${contextPath}/resources/js/app/PostService.js"></script>
</body>
</html>

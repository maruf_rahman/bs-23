package com.bs.travel.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bs.travel.user.model.User;

public interface IUserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}

package com.bs.travel.user.service;

import java.util.List;

import com.bs.travel.post.model.Post;
import com.bs.travel.user.model.User;

public interface IUserService {
    void save(User user);

    User findByUsername(String username);
    
    List<Post> getAllHomePosts();
}

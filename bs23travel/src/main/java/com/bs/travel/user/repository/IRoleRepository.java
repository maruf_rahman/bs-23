package com.bs.travel.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bs.travel.user.model.Role;

public interface IRoleRepository extends JpaRepository<Role, Long>{
}

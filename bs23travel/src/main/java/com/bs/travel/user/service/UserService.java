package com.bs.travel.user.service;

import com.bs.travel.post.model.Post;
import com.bs.travel.post.repository.IPostRepository;
import com.bs.travel.user.model.User;
import com.bs.travel.user.repository.IRoleRepository;
import com.bs.travel.user.repository.IUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {
	@Autowired
	private IPostRepository postRepository;

	@Autowired
	private IUserRepository userRepository;

	@Autowired
	private IRoleRepository roleRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public void save(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setRoles(new HashSet<>(roleRepository.findAll()));
		user.setIsEnable(true);
		userRepository.save(user);
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public List<Post> getAllHomePosts() {
		List<Post> postList = postRepository.findAll().stream()
				.filter(obj -> obj.getPrivacy().getId() == com.bs.travel.enumeration.Privacy.PUBLIC.id())
				.sorted(Comparator.comparing(Post::getDate, Comparator.nullsLast(Comparator.reverseOrder())))
				.collect(Collectors.toList());
		return postList;
	}
}

package com.bs.travel.user.service;

public interface ISecurityService {
    String findLoggedInUsername();

    void autoLogin(String username, String password);
}

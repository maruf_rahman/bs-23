package com.bs.travel.enumeration;

public enum Privacy {
	
	PUBLIC(1l, "Public"),
	PRIVATE(2l, "Private");
	
	private Long id;
	private String value;
	public Long id() {
		return id;
	}
	
	public String value() {
		return value;
	}
	
	private Privacy(Long id, String value) {
		this.id = id;
		this.value = value;
	}
}

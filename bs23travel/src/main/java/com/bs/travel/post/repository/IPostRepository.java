package com.bs.travel.post.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bs.travel.post.model.Post;

@Repository
public interface IPostRepository extends JpaRepository<Post, Long> {

	@Modifying
	@Transactional
	@Query(value = "update post set content = ?2, location_id= ?3, privacy_id = ?4  where id = ?1 and username = ?5", nativeQuery = true)
	public void update(Long id, String content, Long locationID, Long privacyID, String username);

	@Modifying
	@Transactional
	@Query(value = "update post set pin_post = ?2 where id = ?1 and username = ?3", nativeQuery = true)
	public void updatePostPin(Long id, Boolean pin, String username);

	public List<Post> findByUsername(String username);
}

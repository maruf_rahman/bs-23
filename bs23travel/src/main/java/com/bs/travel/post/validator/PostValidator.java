package com.bs.travel.post.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.bs.travel.post.model.Post;

@Component
public class PostValidator implements Validator {

	@Override
	public boolean supports(Class<?> aClass) {
		return Post.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "content", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "privacy", "NotEmpty");
		
	}

}

package com.bs.travel.post.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.bs.travel.common.model.Location;
import com.bs.travel.common.model.Privacy;

@Entity
@Table(name="post")
public class Post {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@NotNull(message = "Content is mandatory")
	@NotEmpty(message = "Content is mandatory")
	@Column(columnDefinition = "TEXT")
	private String content;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="location_id")
	private Location location;
	
	@Transient
	@NotNull(message = "Location is mandatory")
	private Long locationID;
	
	@Transient
	private String locationName;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "privacy_id")
	private Privacy privacy;
	
	@NotNull(message = "Privacy is mandatory")
	@Transient
	private Long privacyID;
	
	@Transient
	private String privacyName;
	
	@Transient
	private Boolean editable;
	
	private Boolean pinPost;
	
	private String username;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Privacy getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Privacy privacy) {
		this.privacy = privacy;
	}

	public Boolean getPinPost() {
		return pinPost;
	}

	public void setPinPost(Boolean pinPost) {
		this.pinPost = pinPost;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getLocationID() {
		return locationID;
	}

	public void setLocationID(Long locationID) {
		this.locationID = locationID;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Long getPrivacyID() {
		return privacyID;
	}

	public void setPrivacyID(Long privacyID) {
		this.privacyID = privacyID;
	}

	public String getPrivacyName() {
		return privacyName;
	}

	public void setPrivacyName(String privacyName) {
		this.privacyName = privacyName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getEditable() {
		return editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}
	
	
}

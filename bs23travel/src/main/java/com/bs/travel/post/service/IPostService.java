package com.bs.travel.post.service;

import java.util.List;

import com.bs.travel.post.model.Post;

public interface IPostService {
	public Post getPostByID(Long postID);
	public List<Post> getPostList(String username);
	public void save(Post post);
	public void pinPost(String username, Long postID, Boolean pin);
}

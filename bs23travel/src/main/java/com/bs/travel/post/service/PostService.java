package com.bs.travel.post.service;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bs.travel.common.model.Location;
import com.bs.travel.common.model.Privacy;
import com.bs.travel.post.model.Post;
import com.bs.travel.post.repository.IPostRepository;

@Service
public class PostService implements IPostService {

	@Autowired
	private IPostRepository postRepository;

	@Override
	public Post getPostByID(Long postID) {
		return postRepository.findById(postID).get();
	}

	@Override
	public List<Post> getPostList(String username) {
		List<Post> postList = postRepository.findByUsername(username);
		postList.stream().forEach(obj -> {
			obj.setLocationID(obj.getLocation().getId());
			obj.setLocationName(obj.getLocation().getName());
			obj.setPrivacyID(obj.getPrivacy().getId());
			obj.setPrivacyName(obj.getPrivacy().getName());
			obj.setEditable(obj.getUsername().equals(username));
		});

		return postList.stream().sorted(Comparator.comparing(Post::getDate, Comparator.nullsLast(Comparator.reverseOrder())))
				.sorted(Comparator.comparing(Post::getPinPost, Comparator.nullsLast(Comparator.reverseOrder())))
				.collect(Collectors.toList());
	}

	@Modifying
	@Transactional
	@Override
	public void save(Post post) {
		if (post.getId() == null) {
			post.setDate(new Date());
			post.setLocation(new Location(post.getLocationID()));
			post.setPrivacy(new Privacy(post.getPrivacyID()));
			postRepository.save(post);
		} else {
			postRepository.update(post.getId(), post.getContent(), post.getLocationID(), post.getPrivacyID(), post.getUsername());
		}
	}

	@Override
	public void pinPost(String username, Long postID, Boolean pinPost) {
		pinPost = pinPost == null ? true : !pinPost;
		postRepository.updatePostPin(postID, pinPost, username);
	}

}

package com.bs.travel.post.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bs.travel.common.model.BaseResponse;
import com.bs.travel.common.service.IUtilService;
import com.bs.travel.post.model.Post;
import com.bs.travel.post.service.IPostService;

@Controller
public class PostController {
	@Autowired
	private IPostService postService;

	@Autowired
	private IUtilService utilService;

	@GetMapping("/post")
	public String post(Model model) {

		model.addAttribute("locationList", utilService.getLocationList());
		model.addAttribute("privacyList", utilService.getPrivacyList());
		return "post";
	}

	@ResponseBody
	@PostMapping("/post/save")
	public ResponseEntity<BaseResponse> save(@Valid @RequestBody Post postDto, HttpServletRequest request) {
		String username = request.getUserPrincipal().getName();
		postDto.setUsername(username);
		postService.save(postDto);
		return new ResponseEntity<BaseResponse>(new BaseResponse(), HttpStatus.OK);
	}

	@ResponseBody
	@GetMapping("/post/{postID}")
	public Post getPostByID(@PathVariable Long postID) {
		return postService.getPostByID(postID);
	}
	
	@ResponseBody
	@PostMapping("/post/pin")
	public void pinPost(@RequestBody Post postDto, HttpServletRequest request) {
		String username = request.getUserPrincipal().getName();
		postService.pinPost(username, postDto.getId(), postDto.getPinPost());
	}

	@ResponseBody
	@GetMapping("/post/list")
	public List<Post> getPostList(HttpServletRequest request) {
		String username = request.getUserPrincipal().getName();
		return postService.getPostList(username);
	}
}

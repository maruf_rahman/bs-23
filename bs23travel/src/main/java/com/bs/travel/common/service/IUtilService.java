package com.bs.travel.common.service;

import java.util.List;

import com.bs.travel.common.model.Location;
import com.bs.travel.common.model.Privacy;

public interface IUtilService {
	public List<Location> getLocationList();
	
	public List<Privacy> getPrivacyList();
}

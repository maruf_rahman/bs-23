package com.bs.travel.common.exception;

import org.springframework.web.bind.ServletRequestBindingException;

public class CustomException extends ServletRequestBindingException{

	private static final long serialVersionUID = 1L;

	public CustomException(String msg) {
        super(msg);
    }
    
}

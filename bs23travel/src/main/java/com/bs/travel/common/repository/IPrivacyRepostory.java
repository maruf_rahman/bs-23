package com.bs.travel.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bs.travel.common.model.Privacy;

@Repository
public interface IPrivacyRepostory  extends JpaRepository<Privacy, Long> {

}

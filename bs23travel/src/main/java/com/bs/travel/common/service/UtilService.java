package com.bs.travel.common.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bs.travel.common.model.Location;
import com.bs.travel.common.model.Privacy;
import com.bs.travel.common.repository.ILocationRepository;
import com.bs.travel.common.repository.IPrivacyRepostory;

@Service
public class UtilService implements IUtilService{

	@Autowired
	private ILocationRepository locationRepository;
	
	@Autowired
	private IPrivacyRepostory privacyRepository;
	
	@Override
	public List<Location> getLocationList() {
		return locationRepository.findAll();
	}

	@Override
	public List<Privacy> getPrivacyList() {
		return privacyRepository.findAll();
	}

}

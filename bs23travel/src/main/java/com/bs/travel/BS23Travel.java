package com.bs.travel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BS23Travel {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(BS23Travel.class, args);
    }
}

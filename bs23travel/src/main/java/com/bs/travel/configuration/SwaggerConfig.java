package com.bs.travel.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    
    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.exam.bs23"))
                .build()
                .apiInfo(metaData());
    }
    
        private ApiInfo metaData() {
        ApiInfo apiInfo = new ApiInfo(
                "BS23 Travel Agency",
                "BS23 Travel Agency",
                "1.0",
                "Terms of service",
                new Contact("Brain Station 23", "http://www.google.com/", "maruf.cse.cou@gmail.com"),
               "Brain Station 23",
                "http://www.google.com/");
        return apiInfo;
    }
}

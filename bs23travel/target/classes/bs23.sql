create database if not exists bs23;

insert into location (id, name) values(1, 'Sylhet');
insert into location (id, name) values(2, 'Bandarban');
insert into location (id, name) values(3, 'Khulna');

insert into privacy (id, name) values(1, 'Public');
insert into privacy (id, name) values(2, 'Private');